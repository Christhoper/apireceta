package com.example.apirecetas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView tvNombreComida, tvListar;
    private Button btCalcular;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvNombreComida = findViewById(R.id.tvNombreComida);
        this.btCalcular = findViewById(R.id.btCalcular);
        this.tvListar = findViewById(R.id.tvListar);

        this.btCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String producto = tvNombreComida.getText().toString().trim();


                String recetaUrl = "https://api.edamam.com/search?q=p="+producto+"&app_id=a0e54b30&app_key=5f0713f1b367f074b1f1db99108337f8";
                StringRequest receta = new StringRequest(
                        Request.Method.GET,
                        recetaUrl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);

                                    JSONArray hits = respuestaJSON.getJSONArray("hits");
                                    JSONObject indiceReceta = hits.getJSONObject(0);
                                    JSONObject recipe = indiceReceta.getJSONObject("recipe");
                                    JSONArray ingredientLines = recipe.getJSONArray("ingredientLines");

                                    String receta = "";

                                    for (int i=0; i<ingredientLines.length(); i++){
                                        receta += ingredientLines.getString(i )+ "\n";

                                        tvListar.setText(receta);
                                    }
                                    Log.d("TAG_INGREDIENTES", "INGREDIENTES: " + receta);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Fallo
                            }
                        }
                );

                RequestQueue listaEsperaReceta = Volley.newRequestQueue(getApplicationContext());
                listaEsperaReceta.add(receta);

            }
        });


    }
}
